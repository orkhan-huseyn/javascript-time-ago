import TimeAgo from '../..'
import az from '../../locale/az'

TimeAgo.addLocale(az)

describe('locale/az', () => {
	it('should format "now"', () => {
		const timeAgo = new TimeAgo('az')
		timeAgo.format(Date.now()).should.equal('elə indicə')
		timeAgo.format(Date.now() + 100).should.equal('bir az sonra')
	})
})